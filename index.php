<?php

// Change a gif from light background to dark background

// You will need to install "gifsicle" ("brew install gifsicle" on mac)

header('Content-type: text/plain');

if (!isset($_GET['path'])) {
	exit('Must supply path parameter, a file path to the file to alter');
}

$path = $_GET['path'];

if (!is_file($path)) {
	exit('Not found');
}

$tempCopy = tempnam(sys_get_temp_dir(), 'gif_background_changer');
copy($path, $tempCopy);
$cmd = 'gifsicle --unoptimize '.escapeshellarg($path).' -o '.escapeshellarg($tempCopy);
shell_exec($cmd);
$path = $tempCopy;

error_reporting(E_ALL);
ini_set('display_errors', '1');

chdir(dirname(__FILE__));

require('lib.php');
require('gif-endec-master/src/Color.php');
require('gif-endec-master/src/DecoderInterface.php');
require('gif-endec-master/src/Decoder.php');
require('gif-endec-master/src/Encoder.php');
require('gif-endec-master/src/Frame.php');
require('gif-endec-master/src/MemoryStream.php');
require('gif-endec-master/src/Renderer.php');
require('gif-endec-master/src/Geometry/Point.php');
require('gif-endec-master/src/Geometry/Rectangle.php');
require('GIFEncoder.class.php');

use GIFEndec\MemoryStream;
use GIFEndec\Decoder;
use GIFEndec\Frame;
use GIFEndec\Encoder;

// Load GIF to MemoryStream
$gifStream = new MemoryStream();
$gifStream->loadFromFile($path);

// Create Decoder instance from MemoryStream
global $gifDecoder;
$gifDecoder = new Decoder($gifStream);

global $tempFilesOld, $tempFilesNew;
$tempFilesOld = array();
$tempFilesNew = array();

global $frames;
$frames = array();

// Run decoder. Pass callback function to process decoded Frames when they're ready.
$gifDecoder->decode(function (Frame $frame, $index) {
	global $gifDecoder;
	global $tempFilesOld, $tempFilesNew;
	global $frames;

	$frames[] = $frame;

	$gd = $frame->createGDImage();

	$trans = imagecolortransparent($gd);
	$transColors = imagecolorsforindex($gd, $trans);

	$width = imagesx($gd);
	$height = imagesy($gd);

	$size = $gifDecoder->getScreenSize();
	$real_width = $size->getWidth();
	$real_height = $size->getHeight();
	$offset = $frame->getOffset();
	$offset_x=$offset->getX();
	$offset_y=$offset->getY();

	$gdNew = imagecreatetruecolor($real_width, $real_height);
	$transNew = imagecolorallocatealpha($gdNew, 127, 0, 127, 127);
	imagefill($gdNew, 0, 0, $transNew);
	imagecolortransparent($gdNew, $transNew);
	imagealphablending($gdNew, false);
	imagesavealpha($gdNew, true);

	// Make adjustments...

	for ($y = 0; $y < $height; $y++) {
		for ($x = 0; $x < $width; $x++) {
			$phpColorValue = imagecolorat($gd, $x, $y);
			$colors = imagecolorsforindex($gd, $phpColorValue);

			if ($colors['alpha'] == 0) { // Not transparent
				$hasAdjacentTransparent = false;
				for ($_y = $y - 1; $_y <= $y + 1; $_y++) {
					for ($_x = $x - 1; $_x <= $x + 1; $_x++) {
						if ($_x < 0 || $_y < 0 || $_x >= $width || $_y >= $height) {
							$isOnLargerCanvas = $_x + $offset_x > 0 && $_y + $offset_y > 0 && $_x + $offset_x < $real_width && $_y + $offset_y < $real_height;
							$isOnLargerCanvas = true; // Actually we always want it, as edges are usually even blended with off-canvas
							if ($isOnLargerCanvas) {
								$hasAdjacentTransparent = true; // Transparent because a neighbour is off-canvas for the frame but on-canvas for the combined image
							}
						}
						elseif ($_x != $x || $_y != $y) {
							$_phpColorValue = imagecolorat($gd, $_x, $_y);
							$_colors = imagecolorsforindex($gd, $_phpColorValue);
							if ($_colors['alpha'] != 0) {
								$hasAdjacentTransparent = true; // Transparent neighbour
							}
						}
					}
				}
				$madeChange = false;
				$r = $colors['red'];
				$g = $colors['green'];
				$b = $colors['blue'];
				if ($hasAdjacentTransparent) {
					list($h, $s, $v) = rgb_to_hsv($r, $g, $b);
					if ($v > 127) {
						$v = 255 - $v;
						list($_r, $_g, $_b) = hsv_to_rgb($h, $s, $v);
						$newPhpColorValue = imagecolorallocatealpha($gdNew, $_r, $_g, $_b, $colors['alpha']);
						if ($newPhpColorValue === false) {
							exit('Error allocating color');
						}
						imagesetpixel($gdNew, $x + $offset_x, $y + $offset_y, $newPhpColorValue);
						$madeChange = true;
					}
				}
				if (!$madeChange) {
					$newPhpColorValue = ($colors['alpha'] == 127) ? $transNew : imagecolorallocatealpha($gdNew, $r, $g, $b, $colors['alpha']);
					imagesetpixel($gdNew, $x + $offset_x, $y + $offset_y, $newPhpColorValue);
				}
			}
		}
	}

	// Save original frame, in case this is useful for debugging
	$tempFileOld = tempnam(sys_get_temp_dir(), 'gif_background_changer');
	$tempFilesOld[] = $tempFileOld;
	imagegif($gd, $tempFileOld);

	// Write back out
	$tempFileNew = tempnam(sys_get_temp_dir(), 'gif_background_changer');
	$tempFilesNew[] = $tempFileNew;
	imagegif($gdNew, $tempFileNew);
});

// Output
/*
Unfortunately this library doesn't work, something wrong with color table encodings
$gifEncoder = new Encoder();
foreach ($tempFilesNew as $i => $tempFile) {
	$frame = $frames[$i];
	$stream = new MemoryStream();
	$stream->loadFromFile($tempFile);
	$frame->setStream($stream);
	$gifEncoder->addFrame($frame);
}
$gifEncoder->addFooter(); // Required after you're done with adding frames
*/
$frame_data = array();
$frame_durations = array();
$frame_disposals = array();
foreach ($tempFilesNew as $i => $tempFile) {
	$frame = $frames[$i];

	$frame_durations[] = $frame->getDuration();

	$GIF_dis = $frame->getDisposalMethod();
	$GIF_dis = ( $GIF_dis > -1 ) ? ( ( $GIF_dis < 3 ) ? $GIF_dis : 3 ) : 2;
	$frame_disposals[] = $GIF_dis;

	$frame_data[] = file_get_contents($tempFile);
}
$gifEncoder = new GIFEncoder(
	$frame_data,
	$frame_durations,
	0,
	$frame_disposals,
	127, 0, 127,
	'bin'
);

// Output
file_put_contents($tempCopy, $gifEncoder->GetAnimation());
/*
$phpStream = $gifEncoder->getStream()->getPhpStream();
rewind($phpStream);
file_put_contents($tempCopy, stream_get_contents($phpStream));
*/
shell_exec('gifsicle -O3 ' . escapeshellarg($tempCopy) . ' -o ' . escapeshellarg($tempCopy));
if (empty($_GET['instant_view'])) {
	header('Content-type: application/octet-stream');
	header('Content-Disposition: attachment; filename="' . basename($_GET['path']) . '"');
	echo file_get_contents($tempCopy);
} else {
	header('Content-type: text/html');
	echo '<body style="background: black">';
	echo '<img src="data:image/gif;base64,' . base64_encode(file_get_contents($tempCopy)) . '" />';
	echo '</body>';
}

// Cleanup
foreach ($tempFilesOld as $tempFile) {
	@unlink($tempFile);
}
foreach ($tempFilesNew as $tempFile) {
	@unlink($tempFile);
}
@unlink($tempCopy);
