<?php

function rgb_to_hsv($red, $green, $blue)
{
    $r = $red / 255.0;
    $g = $green / 255.0;
    $b = $blue / 255.0;
    $h = 0.0;
    $s = 0.0;
    $v = 0.0;
    $min = min($r, $g, $b);
    $max = max($r, $g, $b);
    $delta = $max - $min;

    $v = $max;

    if ($delta == 0.0) {
        $h = 0.0;
        $s = 0.0;
    } else {
        $s = $delta / $max;

        $d_r = ((($max - $r) / 6) + ($delta / 2)) / $delta;
        $d_g = ((($max - $g) / 6) + ($delta / 2)) / $delta;
        $d_b = ((($max - $b) / 6) + ($delta / 2)) / $delta;

        if ($r == $max) {
            $h = $d_b - $d_g;
        } elseif ($g == $max) {
            $h = (1.0 / 3.0) + $d_r - $d_b;
        } else {
            $h = (2.0 / 3.0) + $d_g - $d_r;
        }

        if ($h < 0.0) {
            $h++;
        } elseif ($h > 1.0) {
            $h--;
        }
    }

    return array(intval(round($h * 255)), intval(round($s * 255)), intval(round($v * 255)));
}

function hsv_to_rgb($h, $s, $v)
{
    $h = 6.0 * $h / 255.0;
    $s = $s / 255.0;
    $v = $v / 255.0;

    if ($s == 0.0) {
        $r = $v;
        $g = $v;
        $b = $v;
    } else {
        $hi = intval(floor($h));
        $f = $h - $hi;
        $p = ($v * (1.0 - $s));
        $q = ($v * (1.0 - ($f * $s)));
        $t = ($v * (1.0 - ((1.0 - $f) * $s)));

        switch ($hi) {
            case 0:
                $r = $v;
                $g = $t;
                $b = $p;
                break;
            case 1:
                $r = $q;
                $g = $v;
                $b = $p;
                break;
            case 2:
                $r = $p;
                $g = $v;
                $b = $t;
                break;
            case 3:
                $r = $p;
                $g = $q;
                $b = $v;
                break;
            case 4:
                $r = $t;
                $g = $p;
                $b = $v;
                break;
            default:
                $r = $v;
                $g = $p;
                $b = $q;
                break;
        }
    }

	return array(
		fix_colour(intval(round($r * 255))),
		fix_colour(intval(round($g * 255))),
		fix_colour(intval(round($b * 255)))
	);
}

/**
 * Make sure a colour component fits within the necessary range (0<=x<256).
 *
 * @param  mixed $x Colour component (float or integer).
 * @param  boolean $hue Whether this is hue (meaning it cycles around)
 * @return integer Constrained colour component.
 */
function fix_colour($x, $hue = false)
{
    if (is_float($x)) {
        $x = intval(round($x));
    }

    if ($hue) {
        while ($x > 255) {
            $x -= 255;
        }
        while ($x < 0) {
            $x += 255;
        }
    } else {
        if ($x > 255) {
            $x = 255;
        }
        if ($x < 0) {
            $x = 0;
        }
    }

    return $x;
}
